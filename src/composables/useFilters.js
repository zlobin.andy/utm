import { ref } from "vue";

export const useFilters = (initialFilters = {}) => {
  const filters = ref(initialFilters);

  const resetFilters = () => {
    filters.value = {};
  };

  const setFilters = (value) => {
    filters.value = value;
  };

  return { filters, setFilters, resetFilters };
};
