import { ref } from "vue";

export const useLoading = (initialLoading = false) => {
  const isLoading = ref(initialLoading);

  const setLoading = (status) => (isLoading.value = status);

  return { isLoading, setLoading };
};
