import { computed, ref } from "vue";

export const useError = () => {
  const error = ref(null);
  const isError = computed(() => {
    return Boolean(error.value);
  });

  const setError = (err) => {
    error.value = err;
  };

  const resetError = () => {
    if (error.value !== null) {
      error.value = null;
    }
  };

  return { setError, resetError, isError, error };
};
