export const FILTER_STATUS = {
  all: "all",
  active: "active",
  archive: "archive",
};

export const FILTER_ARCHIVE_VALUE = {
  [FILTER_STATUS.all]: undefined,
  [FILTER_STATUS.active]: false,
  [FILTER_STATUS.archive]: true,
};
