export const linksRoutes = [
  {
    path: "/tracker",
    name: "links",
    component: () => import("@/modules/Tracker/views/TrackerView.vue"),
    children: [
      {
        path: "/tracker",
        name: "links-list",
        component: () => import("@/modules/Tracker/views/LinksView.vue"),
      },
      {
        path: "/tracker/:domain/:uid",
        name: "link-statistic",
        component: () =>
          import("@/modules/Tracker/views/LinksStatisticView.vue"),
      },
      {
        path: "/tracker/new",
        name: "add-links",
        component: () => import("@/modules/Tracker/views/AddLinkView.vue"),
      },
    ],
  },
];
