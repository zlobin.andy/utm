const BASE_API_URL = "http://194.58.96.208:3131/api/v1";

export const API_LINKS_ENDPOINTS = {
  getLinks: `${BASE_API_URL}/trackers`,
  getLink: (id) => `${BASE_API_URL}/trackers/${id}`,
  createLink: `${BASE_API_URL}/trackers`,
  archiveLink: (id) => `${BASE_API_URL}/trackers/${id}/archive`,
  getRedirect: (id) => `${BASE_API_URL}/trackers/test/${id}`,
  getStatistic: (id) => `${BASE_API_URL}/trackers/${id}/stats`,
};
