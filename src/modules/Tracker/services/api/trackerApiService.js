const defaultPostHeaders = {
  "content-type": "application/json",
};
export class TrackerApiService {
  static async fetchLinks(url) {
    try {
      const response = await fetch(url);
      const data = await response.json();
      return data.result;
    } catch (e) {
      return Promise.reject(e);
    }
  }
  static async fetchLink(url) {
    try {
      const response = await fetch(url);
      return await response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }
  static async fetchLinkStatistic(url) {
    try {
      const response = await fetch(url);
      return await response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }
  static async createLink(url, body) {
    try {
      const response = await fetch(url, {
        method: "post",
        headers: defaultPostHeaders,
        body: JSON.stringify(body),
      });
      return await response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }

  static async setLinkArchiveStatus(url) {
    try {
      const response = await fetch(url, {
        method: "POST",
        headers: defaultPostHeaders,
        body: JSON.stringify({ isArchived: true }),
      });
      return await response.json();
    } catch (e) {
      return Promise.reject(e);
    }
  }
}
