import { defineStore } from "pinia";
import { ref } from "vue";
import { useError, useLoading } from "@/composables";
import { createUrlLink } from "@/modules/Tracker/utils";
import {
  API_LINKS_ENDPOINTS,
  TrackerApiService,
} from "@/modules/Tracker/services/api";
import { useTrackerStore } from "@/modules/Tracker/stores/trackerStore";

export const useLinkStore = defineStore("link", () => {
  const link = ref({});
  const statistic = ref({});
  const { isLoading, setLoading } = useLoading();
  const { isError, setError, resetError } = useError();
  const { getLinks } = useTrackerStore();

  const getLink = async (id) => {
    try {
      const url = createUrlLink(API_LINKS_ENDPOINTS.getLink(id));
      const result = await TrackerApiService.fetchLink(url);
      console.log(result);
      link.value = result.data;
    } catch (e) {
      return Promise.reject(e);
    }
  };

  const getLinkStatistic = async (id) => {
    try {
      const url = createUrlLink(API_LINKS_ENDPOINTS.getStatistic(id));
      const result = await TrackerApiService.fetchLinkStatistic(url);
      console.log(result.data);
      statistic.value = result.data;
    } catch (e) {
      return Promise.reject(e);
    }
  };

  const setLinkToArchive = async (id) => {
    try {
      const url = createUrlLink(API_LINKS_ENDPOINTS.archiveLink(id));
      await TrackerApiService.setLinkArchiveStatus(url);
      await getLinks();
    } catch (e) {
      return Promise.reject(e);
    }
  };

  const getLinkData = async (id) => {
    setLoading(true);
    resetError();
    try {
      await Promise.all([getLinkStatistic(id), getLink(id)]);
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }
  };

  const createNewLink = async (body) => {
    try {
      const url = createUrlLink(API_LINKS_ENDPOINTS.createLink);
      return await TrackerApiService.createLink(url, body);
    } catch (e) {
      return Promise.reject(e);
    }
  };

  return {
    link,
    statistic,
    isLoading,
    isError,
    getLinkData,
    createNewLink,
    getLinkStatistic,
    setLinkToArchive,
  };
});
