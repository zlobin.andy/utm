import { computed, ref } from "vue";
import { defineStore } from "pinia";
import { useError, useLoading } from "@/composables";
import { API_LINKS_ENDPOINTS, TrackerApiService } from "../services/api";
import { FILTER_STATUS, FILTER_ARCHIVE_VALUE } from "../constants";
import { createUrlLink, getLinkFilter } from "../utils";

export const useTrackerStore = defineStore("tracker", () => {
  const links = ref([]);
  const filterStatus = ref(FILTER_STATUS.all);
  const { isLoading, setLoading } = useLoading();
  const { isError, setError, resetError } = useError();

  const filteredLinks = computed(() => {
    const filterState = getLinkFilter(filterStatus.value);
    return links.value.filter(filterState);
  });

  const filter = computed(() => {
    return filterStatus.value !== FILTER_STATUS.all
      ? { archive: FILTER_ARCHIVE_VALUE[filterStatus.value] }
      : {};
  });

  const getLinks = async () => {
    const url = createUrlLink(API_LINKS_ENDPOINTS.getLinks, filter.value);
    setLoading(true);
    resetError();
    try {
      const data = await TrackerApiService.fetchLinks(url);
      links.value = data;
    } catch (e) {
      setError(e);
    } finally {
      setLoading(false);
    }
  };

  const changeFilterStatus = async (status) => {
    filterStatus.value = status;
    await getLinks();
  };

  return {
    links,
    isLoading,
    isError,
    filteredLinks,
    filterStatus,
    getLinks,
    changeFilterStatus,
  };
});
