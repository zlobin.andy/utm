import { FILTER_STATUS } from "../constants";

export const getLinkFilter = (status) => {
  return (item) => {
    const state = {
      [FILTER_STATUS.all]: item,
      [FILTER_STATUS.active]: !item.isArchived,
      [FILTER_STATUS.archive]: item.isArchived,
    };
    return state[status];
  };
};
