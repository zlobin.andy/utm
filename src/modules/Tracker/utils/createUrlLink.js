/**
 * @param {String} - base url
 * @param {Object}
 * @return URL
 * */
export const createUrlLink = (url, params = {}) => {
  const link = new URL(url);
  if (Object.keys(params).length > 0) {
    Object.keys(params).forEach((key) => {
      link.searchParams.append(key, `${params[key]}`);
    });
  }

  return link;
};
