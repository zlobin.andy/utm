import { createRouter, createWebHistory, RouterView } from "vue-router";
import { linksRoutes } from "@/modules/Tracker/router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "home",
      component: RouterView,
    },
    ...linksRoutes,
  ],
});

export default router;
